# MultiRoomWindows

Exemplary scripts to switch primary display and audio source on windows host

## Prerequisites

### MultiMonitorTool
Download and install from: https://www.nirsoft.net/utils/multi_monitor_tool.html

After installation add path to: `MultiMonitorTool.exe` to **Path** Environment Variable

### AudioDeviceCmdlets
Download and install from: https://github.com/frgnca/AudioDeviceCmdlets/tree/v3.0

### AutoHotKey
Download and install from: https://www.autohotkey.com/

To allow Powershell script execution (run as Administrator)
```powershell
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
Set-ExecutionPolicy RemoteSigned -Scope LocalMachine
```

## Usage
Modify `Switch*.bat` and `SetAudio*.ps1` to your needs

Assign keyboard shortcut to `.bat` files in AutoHotKey
